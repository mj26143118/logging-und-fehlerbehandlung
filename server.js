const express = require('express');
const morgan = require('morgan');

const app = express();
const port = 3000;

app.use(morgan('combined'));

app.use(express.json());

// Routes
app.get('/', (req, res) => {
  res.send('Hi, Express Grundlagen 5: Logging und Fehlerbehandlung!, Mojtaba Pourshiri');
});

app.get('/data', (req, res) => {
  res.json({ message: 'GET request to /data' });
});

app.post('/data', (req, res) => {
  res.json({ message: 'POST request to /data', body: req.body });
});

app.put('/data', (req, res) => {
  res.json({ message: 'PUT request to /data', body: req.body });
});

app.delete('/data', (req, res) => {
  res.json({ message: 'DELETE request to /data' });
});

//  error handling middleware
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ error: 'Something went wrong!' });
});

app.use((req, res, next) => {
  res.status(404).json({ error: 'Not Found' });
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
